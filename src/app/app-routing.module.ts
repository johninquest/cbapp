import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartComponent } from './start/start.component';
import { TransactionComponent } from './transaction/transaction.component';
import { PersonsComponent } from './persons/persons.component';
import { SummaryComponent } from './summary/summary.component';
import { from } from 'rxjs';

const routes: Routes = [
  { path: '', redirectTo: '/start', pathMatch: 'full' },
  { path: 'start', component: StartComponent },
  { path: 'transaction', component: TransactionComponent },
  { path: 'persons', component: PersonsComponent },
  { path: 'summary', component: SummaryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
