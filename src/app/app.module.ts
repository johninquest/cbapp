import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModules } from './materialmodule';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StartComponent } from './start/start.component';
import { TransactionComponent } from './transaction/transaction.component';
import { PersonsComponent } from './persons/persons.component';
import { SummaryComponent } from './summary/summary.component';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    StartComponent,
    TransactionComponent,
    PersonsComponent,
    SummaryComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModules,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
