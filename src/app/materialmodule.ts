import {NgModule} from '@angular/core';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatNativeDateModule,
    MatDatepickerModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatStepperModule} from '@angular/material/stepper';
/*import { MatMomentDateModule } from "@angular/material-moment-adapter";*/
import { from } from 'rxjs';

@NgModule({
    imports: [
        MatButtonModule,
        MatCheckboxModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        MatMenuModule,
        MatSelectModule,
        MatListModule,
        MatIconModule,
        MatDatepickerModule,
        MatTableModule,
        MatCardModule,
        MatDividerModule,
        MatStepperModule
    ],
    exports: [
        MatButtonModule,
        MatCheckboxModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        MatMenuModule,
        MatSelectModule,
        MatListModule,
        MatIconModule,
        MatDatepickerModule,
        MatTableModule,
        MatCardModule,
        MatDividerModule,
        MatStepperModule
    ],
    providers: [ MatDatepickerModule ]
})
export class MaterialModules { }

